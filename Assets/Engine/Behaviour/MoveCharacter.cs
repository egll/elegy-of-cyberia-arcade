﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacter : MonoBehaviour {

	public static void StartAction(Rigidbody2D rb, float force)
    {
        rb.velocity = new Vector2(force, rb.velocity.y);
    }

    public static MOVEMENT Facing(Rigidbody2D rb)
    {   
        if(rb.velocity.x == 0)
        {
            return MOVEMENT.STATIC;
        }
        return rb.velocity.x > 0 ? MOVEMENT.RIGHT : MOVEMENT.LEFT;
    }

    public static bool IsWalking(Rigidbody2D rb)
    {
        return Jump.IsInGround(rb) && Facing(rb) != MOVEMENT.STATIC;
    }
}

public enum MOVEMENT
{
    LEFT,
    RIGHT,
    STATIC
}
