﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutomaticSceneLoading : MonoBehaviour
{
    public float delaySeconds = 3f;
    public int sceneIndex;
    public List<ScriptableObject> avoidingGarbageCollector;
    void Start()
    {
        StartCoroutine(NextScene());
    }

    private IEnumerator NextScene()
    {
        yield return new WaitForSeconds(delaySeconds);
        SceneManager.LoadScene(sceneIndex);
    }
}
