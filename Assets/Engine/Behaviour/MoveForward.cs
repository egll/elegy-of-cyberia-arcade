﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    public float force = 10f;
    public float damage = 35f;
    public float lifetime = 5f;
    Rigidbody2D rb;
    public LayerMask groundLayer;
    public ParticleSystem impact;
    public ParticleSystem bloodImpact;
    

    private void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * force;
        Destroy(gameObject, lifetime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsObjectFromLayer(collision.gameObject))
        {
            Instantiate(impact, transform.position,Quaternion.identity);
            gameObject.SetActive(false);
        }
        if (collision.gameObject.tag == "P2")
        {
            gameObject.SetActive(false);
            Instantiate(bloodImpact, transform.position, Quaternion.identity);
            collision.gameObject.GetComponent<PlayerStats>().TakeDamage(damage);
        }
        if (collision.gameObject.tag == "Sheep")
        {
            gameObject.SetActive(false);
            Debug.Log("Test");
            collision.gameObject.GetComponent<Sheep>().TakeDamage(damage);
        }
    }

    private bool IsObjectFromLayer(GameObject other)
    {
        return (groundLayer.value & 1 << other.layer) != 0;
    }

}
