﻿using Sound.RandomController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump
{

    public static bool StartAction(Rigidbody2D rb, float jumpForce)
    {
        if (IsInGround(rb))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            return true;
        }
        return false;
    }

    public static bool IsInGround(Rigidbody2D rb)
    {
        return rb.velocity.y == 0;
    }
}
