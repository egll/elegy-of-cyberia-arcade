﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayersSelection : MonoBehaviour
{
    public SelectedCharacters selection;
    private int assignedCharacters;

    public void AddSelection(int player, PlayerActions actions, GameObject prefab)
    {
        assignedCharacters++;
        if (player == 0)
        {
            //selection.P1Device = actions.Device;
            selection.P1Character = prefab;
            selection.P1Actions = actions;
            selection.P1SortOrder = actions.Device.SortOrder;
        }
        else
        {
            //selection.P2Device = actions.Device;
            selection.P2Actions = actions;
            selection.P2Character = prefab;
            selection.P2SortOrder = actions.Device.SortOrder;
        }
    }

    public void RemoveSelection(int player)
    {
        assignedCharacters--;
    }

    public void FixedUpdate()
    {
        if (assignedCharacters == 2 && DataIsNotNull())
        {
            StartCoroutine(ChangeScene());
        }
    }

    bool DataIsNotNull()
    {
        return PlayerDataIsNotNull(selection.P1Character, selection.P1SortOrder) && PlayerDataIsNotNull(selection.P2Character, selection.P2SortOrder);
    }

    bool PlayerDataIsNotNull(GameObject prefab, int device)
    {
        return prefab != null && device != 0;
    }

    IEnumerator ChangeScene()
    {
        selection.P1Actions.Destroy();
        selection.P2Actions.Destroy();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(1);
    }

}
