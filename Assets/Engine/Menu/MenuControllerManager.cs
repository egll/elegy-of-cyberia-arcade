﻿using InControl;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MenuControllerManager : MonoBehaviour {
    public List<GameObject> cursorSprites;
    private int assignedControllers;
    private List<CursorFocus> characters;
    PlayerActions joystickListener;

    void OnEnable()
    {
        InputManager.OnDeviceDetached += OnDeviceDetached;
        characters = new List<CursorFocus>();
        joystickListener = PlayerActions.CreateJoystickBindings();
    }

    void OnDisable()
    {
        InputManager.OnDeviceDetached -= OnDeviceDetached;
        joystickListener.Destroy();
    }

    void OnDeviceDetached(InputDevice inputDevice)
    {
        var cursor = FindPlayerUsingJoystick(inputDevice);
        if (cursor != null)
        {
            RemovePlayer(cursor);
        }
    }

    void Update()
    {
        if (JoinButtonWasPressedOnListener(joystickListener))
        {
            var inputDevice = InputManager.ActiveDevice;
            if (ThereIsNoPlayerUsingJoystick(inputDevice))
            {

                AssignPlayer(inputDevice);
            }
        }
    }

    bool JoinButtonWasPressedOnListener(PlayerActions actions)
    {
        return actions.Jump.WasPressed || actions.Shoot.WasPressed || actions.Drop.WasPressed;
    }

    private void AssignPlayer(InputDevice inputDevice)
    {
        if (cursorSprites != null && assignedControllers < cursorSprites.Count)
        {
            cursorSprites[assignedControllers].SetActive(true);
            CursorFocus focus = cursorSprites[assignedControllers].GetComponent<CursorFocus>();

            var actions = PlayerActions.CreateJoystickBindings();
            actions.Device = inputDevice;
            focus.Actions = actions;
            focus.assignedPlayer = assignedControllers;

            characters.Add(focus);
            assignedControllers++;
        }
    }

    bool ThereIsNoPlayerUsingJoystick(InputDevice inputDevice)
    {
        return FindPlayerUsingJoystick(inputDevice) == null;
    }

    void RemovePlayer(CursorFocus cursor)
    {
        characters.Remove(cursor);
        cursor.Actions = null;
        Destroy(cursor.gameObject);
    }

    CursorFocus FindPlayerUsingJoystick(InputDevice inputDevice)
    {
        var playerCount = characters.Count;
        for (var i = 0; i < playerCount; i++)
        {
            var player = characters[i];
            if (player.Actions.Device == inputDevice)
            {
                return player;
            }
        }
        return null;
    }
}
