﻿using InControl;

public class PlayerActions : PlayerActionSet
{
    public PlayerAction Jump;
    public PlayerAction Shoot;
    public PlayerAction Drop;
    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerOneAxisAction Movement;

    public PlayerAction AimLeft;
    public PlayerAction AimRight;
    public PlayerAction AimUp;
    public PlayerAction AimDown;
    public PlayerTwoAxisAction AimRotation;

    public PlayerActions()
    {
        Jump = CreatePlayerAction("Jump");
        Jump.StateThreshold = 0.6f;
        Shoot = CreatePlayerAction("Shoot");
        Drop = CreatePlayerAction("Drop");
        Left = CreatePlayerAction("Left");
        Left.StateThreshold = 0.6f;
        Right = CreatePlayerAction("Right");
        Right.StateThreshold = 0.6f;
        Movement = CreateOneAxisPlayerAction(Left, Right);

        AimLeft = CreatePlayerAction("AimLeft");
        AimRight = CreatePlayerAction("AimRight");
        AimUp = CreatePlayerAction("AimUp");
        AimDown = CreatePlayerAction("AimDown");
        AimRotation = CreateTwoAxisPlayerAction(AimLeft, AimRight, AimDown, AimUp);
    }

    public static PlayerActions CreateJoystickBindings()
    {
        var actions = new PlayerActions();
        actions.Jump.AddDefaultBinding(InputControlType.Action1);
        actions.Jump.AddDefaultBinding(InputControlType.LeftStickUp);
        actions.Drop.AddDefaultBinding(InputControlType.Action2);
        actions.Shoot.AddDefaultBinding(InputControlType.RightTrigger);
        actions.Left.AddDefaultBinding(InputControlType.LeftStickLeft);
        actions.Right.AddDefaultBinding(InputControlType.LeftStickRight);
        actions.Jump.AddDefaultBinding(InputControlType.DPadUp);
        actions.Left.AddDefaultBinding(InputControlType.DPadLeft);
        actions.Right.AddDefaultBinding(InputControlType.DPadRight);

        actions.AimUp.AddDefaultBinding(InputControlType.RightStickUp);
        actions.AimDown.AddDefaultBinding(InputControlType.RightStickDown);
        actions.AimLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        actions.AimRight.AddDefaultBinding(InputControlType.RightStickRight);
        return actions;
    }

    public static PlayerActions CreateKeyboardBindings()
    {
        var actions = new PlayerActions();
        actions.Jump.AddDefaultBinding(Key.W);
        actions.Shoot.AddDefaultBinding(Key.Space);
        actions.Drop.AddDefaultBinding(Key.G);
        actions.Left.AddDefaultBinding(Key.A);
        actions.Right.AddDefaultBinding(Key.D);

        actions.AimLeft.AddDefaultBinding(Key.J);
        actions.AimRight.AddDefaultBinding(Key.L);
        actions.AimDown.AddDefaultBinding(Key.K);
        actions.AimUp.AddDefaultBinding(Key.I);
        return actions;
    }

    public static PlayerActions CreateKeyboardAndMouseBindings()
    {
        var actions = new PlayerActions();
        actions.Jump.AddDefaultBinding(Key.UpArrow);
        actions.Shoot.AddDefaultBinding(Mouse.LeftButton);
        actions.Drop.AddDefaultBinding(Mouse.RightButton);
        actions.Left.AddDefaultBinding(Key.LeftArrow);
        actions.Right.AddDefaultBinding(Key.RightArrow);

        actions.AimLeft.AddDefaultBinding(Mouse.NegativeX);
        actions.AimRight.AddDefaultBinding(Mouse.PositiveX);
        actions.AimDown.AddDefaultBinding(Mouse.NegativeY);
        actions.AimUp.AddDefaultBinding(Mouse.PositiveY);
        return actions;
    }

}