﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorFocus : MonoBehaviour
{
    public PlayerActions Actions { get; set; }
    public int assignedPlayer { get; set; }
    public GameObject focusedCharacter;
    public PlayersSelection selection;
    public GameObject SelectedFX;
    private SelectableCharacter selectedCharacter;
    private bool hasSelected;
    private float selectionDelay = 0.5f;

    private void OnEnable()
    {
        if (focusedCharacter != null)
        {
            gameObject.transform.position = focusedCharacter.transform.position;
            UpdateSelection();
        }
        hasSelected = false;
        //FocusCharacter(selectedCharacter);
    }

    void FixedUpdate()
    {
        FocusCharacter(selectedCharacter);
        if (!hasSelected)
        {
            if (Actions != null)
            {
                if (Actions.Left.WasPressed)
                {
                    focusedCharacter = selectedCharacter.left.gameObject;
                    UpdateSelection();
                }

                if (Actions.Right.WasPressed)
                {
                    focusedCharacter = selectedCharacter.right.gameObject;
                    UpdateSelection();
                }

                if (Actions.Shoot.WasPressed && selectionDelay <= 0)
                {
                    Select();

                }

                if (selectionDelay > 0)
                {
                    selectionDelay -= Time.fixedDeltaTime;
                }
            }
        }
        else
        {
            if (Actions.Drop.WasPressed)
            {
                Deselect();
            }
        }
    }

    void FocusCharacter(SelectableCharacter character)
    {
        if (character != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, character.gameObject.transform.position, Time.deltaTime * 2500.0f);
        }
    }

    void UpdateSelection()
    {
        selectedCharacter = focusedCharacter.GetComponent<SelectableCharacter>();

    }

    void Select()
    {
        hasSelected = true;
        selection.AddSelection(assignedPlayer, Actions, selectedCharacter.CharacterPrefab);
        PlayAnimation();
        AddEffect();
    }

    void Deselect()
    {
        hasSelected = false;
        selection.RemoveSelection(assignedPlayer);
        selectedCharacter.VFX.Stop();
    }

    public void PlayAnimation()
    {
        Animator animator = focusedCharacter.GetComponentInChildren<Animator>();
        if (animator != null)
        {
            animator.SetTrigger("Selected");
        }
    }

    public void AddEffect()
    {
        if (selectedCharacter.VFX != null)
        {
            selectedCharacter.VFX.Play();
            //Instantiate(SelectedFX, new Vector3(focusedCharacter.transform.position.x, focusedCharacter.transform.position.y - 60f), Quaternion.identity);
        }
    }
}
