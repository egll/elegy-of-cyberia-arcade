﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableCharacter : MonoBehaviour {
    public SelectableCharacter left;
    public SelectableCharacter right;
    public GameObject CharacterPrefab;
    public ParticleSystem VFX;

    void Start()
    {
        if (VFX != null)
        {
            VFX.Stop();
        }
    }
}
