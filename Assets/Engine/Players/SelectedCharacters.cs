﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SelectedCharacters : ScriptableObject
{
    [Header("Player 1")]
    public GameObject P1Character;
    //public InputDevice P1Device { get; set; }
    public int P1SortOrder;
    public PlayerActions P1Actions { get; set; }

    [Header("Player 2")]
    public GameObject P2Character;
    //public InputDevice P2Device;
    public int P2SortOrder;
    public PlayerActions P2Actions { get; set; }
}
