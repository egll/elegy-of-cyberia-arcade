﻿using System.Collections.Generic;
using InControl;
using UnityEngine;

public enum ControllerConfiguration
{
    KEYBOARD_MOUSE,
    KEYBOARD,
    CONTROLLER
}

public class PlayerManagerDemo : MonoBehaviour
{
    public GameObject playerPrefab;
    const int maxPlayers = 2;

    List<PlayerController> players = new List<PlayerController>(maxPlayers);
    List<Transform> spawnPoints;

    public bool UsePrefabs = false;
    public GameObject[] spritesInScene;
    private int assignedSprites = 0;

    PlayerActions keyboardListener;
    PlayerActions joystickListener;
    PlayerActions keyboardAndMouseListener;


    void OnEnable()
    {
        InputManager.OnDeviceDetached += OnDeviceDetached;
        keyboardListener = PlayerActions.CreateKeyboardBindings();
        joystickListener = PlayerActions.CreateJoystickBindings();
        keyboardAndMouseListener = PlayerActions.CreateKeyboardAndMouseBindings();
        GetSpawnPoints();
    }

    void OnDisable()
    {
        InputManager.OnDeviceDetached -= OnDeviceDetached;
        joystickListener.Destroy();
        keyboardListener.Destroy();
        keyboardAndMouseListener.Destroy();
    }

    void Update()
    {
        if (JoinButtonWasPressedOnListener(joystickListener))
        {
            var inputDevice = InputManager.ActiveDevice;

            if (ThereIsNoPlayerUsingJoystick(inputDevice))
            {
                AssignOrCreatePlayer(ControllerConfiguration.CONTROLLER, inputDevice);
            }
        }

        if (JoinButtonWasPressedOnListener(keyboardListener))
        {
            if (ThereIsNoPlayerUsingKeyboard())
            {
                AssignOrCreatePlayer(ControllerConfiguration.KEYBOARD, null);
            }
        }

        if (JoinButtonWasPressedOnListener(keyboardAndMouseListener))
        {
            if (ThereIsNoPlayerUsingKeyboardAndMouse())
            {
                AssignOrCreatePlayer(ControllerConfiguration.KEYBOARD_MOUSE, null);
            }
        }
    }

    bool JoinButtonWasPressedOnListener(PlayerActions actions)
    {
        return actions.Jump.WasPressed || actions.Shoot.WasPressed || actions.Drop.WasPressed;
    }

    PlayerController FindPlayerUsingJoystick(InputDevice inputDevice)
    {
        var playerCount = players.Count;
        for (var i = 0; i < playerCount; i++)
        {
            var player = players[i];
            if (player.Actions.Device == inputDevice)
            {
                return player;
            }
        }
        return null;
    }


    bool ThereIsNoPlayerUsingJoystick(InputDevice inputDevice)
    {
        return FindPlayerUsingJoystick(inputDevice) == null;
    }


    PlayerController FindPlayerUsingListener(PlayerActions listener)
    {
        var playerCount = players.Count;
        for (var i = 0; i < playerCount; i++)
        {
            var player = players[i];
            if (player.Actions == listener)
            {
                return player;
            }
        }
        return null;
    }


    bool ThereIsNoPlayerUsingKeyboard()
    {
        return FindPlayerUsingListener(keyboardListener) == null;
    }

    bool ThereIsNoPlayerUsingKeyboardAndMouse()
    {
        return FindPlayerUsingListener(keyboardAndMouseListener) == null;
    }


    void OnDeviceDetached(InputDevice inputDevice)
    {
        var player = FindPlayerUsingJoystick(inputDevice);
        if (player != null)
        {
            RemovePlayer(player);
        }
    }

    void AssignOrCreatePlayer(ControllerConfiguration config, InputDevice inputDevice)
    {
        if (UsePrefabs)
        {
            CreatePlayer(config, inputDevice);
        }
        else
        {
            AssignToSprites(config, inputDevice);
        }
    }

    void AssignToSprites(ControllerConfiguration config, InputDevice inputDevice)
    {
        if (spritesInScene != null && assignedSprites < spritesInScene.Length)
        {
            spritesInScene[assignedSprites].SetActive(true);
            PlayerController player = spritesInScene[assignedSprites].GetComponent<PlayerController>();
            if (config == ControllerConfiguration.KEYBOARD)
            {
                player.Actions = keyboardListener;
            }
            else if (config == ControllerConfiguration.KEYBOARD_MOUSE)
            {
                Cursor.visible = false;
                player.Actions = keyboardAndMouseListener;
            }
            else
            {
                var actions = PlayerActions.CreateJoystickBindings();
                actions.Device = inputDevice;
                player.Actions = actions;
            }
            players.Add(player);
            assignedSprites++;
        }
    }

    PlayerController CreatePlayer(ControllerConfiguration config, InputDevice inputDevice)
    {
        if (players.Count < maxPlayers)
        {
            var playerPosition = spawnPoints[0];
            spawnPoints.RemoveAt(0);

            var gameObject = (GameObject)Instantiate(playerPrefab, playerPosition.position, playerPosition.rotation);
            var player = gameObject.GetComponent<PlayerController>();

            if (config == ControllerConfiguration.KEYBOARD)
            {
                player.Actions = keyboardListener;
            }
            else if (config == ControllerConfiguration.KEYBOARD_MOUSE)
            {
                player.Actions = keyboardAndMouseListener;
            }
            else
            {
                var actions = PlayerActions.CreateJoystickBindings();
                actions.Device = inputDevice;
                player.Actions = actions;
            }

            players.Add(player);

            return player;
        }
        return null;
    }

    void RemovePlayer(PlayerController player)
    {
        spawnPoints.Insert(0, player.gameObject.transform);
        players.Remove(player);
        player.Actions = null;
        Destroy(player.gameObject);
    }

    void GetSpawnPoints()
    {
        var tmp = new List<Transform>();
        GameObject[] spawnsInScene = GameObject.FindGameObjectsWithTag("Spawnpoint");
        for (int i = 0; i < spawnsInScene.Length; i++)
        {
            tmp.Add(spawnsInScene[i].transform);
        }
        spawnPoints = tmp;
    }
}