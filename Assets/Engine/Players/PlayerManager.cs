﻿using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject[] initialPositions;
    public SelectedCharacters playersSelection;
    public bool useScriptableObjectData;
    private List<InputDevice> assignedDevice;
    private bool AllowSetup = true;
    private int postionIndex;

    private void Start()
    {
        AllowSetup = true;
        initialPositions[0].SetActive(false);

        initialPositions[1].SetActive(false);
        assignedDevice = new List<InputDevice>();
    }

    private void FixedUpdate()
    {
        if (AllowSetup)
        {
            InputManager.ClearInputState();
            if (useScriptableObjectData)
            {
                CreateCharacter(playersSelection.P1Character, playersSelection.P1SortOrder, playersSelection.P1Actions);
                CreateCharacter(playersSelection.P2Character, playersSelection.P2SortOrder, playersSelection.P2Actions);
            }
            else
            {
                CreateCharacter(initialPositions[postionIndex].gameObject, playersSelection.P1SortOrder, playersSelection.P1Actions);
                CreateCharacter(initialPositions[postionIndex].gameObject, playersSelection.P1SortOrder, playersSelection.P1Actions);
            }
            AllowSetup = false;
        }

    }

    void CreateCharacter(GameObject prefab, int deviceOrder, PlayerActions actions)
    {
        var player = (GameObject)Instantiate(prefab, initialPositions[postionIndex].transform.position, initialPositions[postionIndex].transform.rotation);
        player.SetActive(true);
        var actionsP = PlayerActions.CreateJoystickBindings();
        actionsP.Device = AssignDevice(deviceOrder);
        player.GetComponent<PlayerController>().Actions = actionsP;
        postionIndex++;
    }

    private InputDevice AssignDevice(int deviceOrder)
    {
        var device = InputManager.ActiveDevice;
        foreach (var randDevice in InputManager.Devices)
        {
            if (!assignedDevice.Contains(randDevice) && (useScriptableObjectData ? randDevice.SortOrder == deviceOrder : true))
            {
                assignedDevice.Add(randDevice);
                device = randDevice;
            }
        }    
        return device;
    }





}
