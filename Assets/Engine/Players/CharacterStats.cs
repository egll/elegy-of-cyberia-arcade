﻿using Sound.RandomController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterStats : ScriptableObject
{
    [Header("Initial Stats")]
    public float InitialSpeed;
    public float InitialHP;
    [Header("Current Stats")]
    public float Speed;
    public float HP;
    public float JumpForce;
    public float RespawnDelay;
    public bool IsAlive;
    public float CharacterScore;
    [Header("Shooting")]
    public float ShootDelay;
    public float AimingRadius;
    public GameObject Bullet;
    public GameObject arrow;


    public void OnEnable()
    {
        ResetStats();
        CharacterScore = 0;
    }

    public void ResetStats()
    {
        Speed = InitialSpeed;
        HP = InitialHP;
        IsAlive = true;
    }
}
