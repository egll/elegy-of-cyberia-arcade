﻿namespace InterfaceMovement
{
    using UnityEngine;


    public class ButtonFocus : MonoBehaviour
    {
        public PlayerActions Actions { get; set; }
        public GameObject focusedButton;

        private void OnEnable()
        {
            gameObject.transform.position = focusedButton.transform.position;
        }

        void Update()
        {
            var buttonComponent = focusedButton.GetComponent<Button>();
            FocusButton(buttonComponent);
            if (Actions != null)
            {
                if (Actions.Left.WasPressed)
                {
                    focusedButton = buttonComponent.left.gameObject;
                }


                if (Actions.Right.WasPressed)
                {
                    focusedButton = buttonComponent.right.gameObject;
                }

                if (Actions.Shoot.WasPressed)
                {
                    Debug.Log(gameObject.name + " has selected " + focusedButton.name);
                }
            }
        }

        void FocusButton(Button button)
        {
            if (button != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, button.gameObject.transform.position, Time.deltaTime * 10.0f);
            }
        }
    }
}
