﻿namespace InterfaceMovement
{
    using InControl;
    using System;
    using System.Collections.Generic;
    using UnityEngine;


    public class ButtonManager : MonoBehaviour
    {
        List<ButtonFocus> buttonsCursor;
        public List<GameObject> playerSprites;
        private int assignedControllers;
        int MaxPlayers = 2;

        PlayerActions joystickListener;

        void OnEnable()
        {
            InputManager.OnDeviceDetached += OnDeviceDetached;
            buttonsCursor = new List<ButtonFocus>();
            joystickListener = PlayerActions.CreateJoystickBindings();

        }

        private void OnDisable()
        {
            InputManager.OnDeviceDetached -= OnDeviceDetached;
            joystickListener.Destroy();
        }


        void Update()
        {
            if (JoinButtonWasPressedOnListener(joystickListener))
            {
                var inputDevice = InputManager.ActiveDevice;
                if (ThereIsNoPlayerUsingJoystick(inputDevice))
                {

                    AssignPlayer(inputDevice);
                }
            }
        }

        bool JoinButtonWasPressedOnListener(PlayerActions actions)
        {
            
            return actions.Jump.WasPressed || actions.Shoot.WasPressed || actions.Drop.WasPressed;
        }

        private void AssignPlayer(InputDevice inputDevice)
        {
            if (playerSprites != null && assignedControllers < playerSprites.Count)
            {
                playerSprites[assignedControllers].SetActive(true);
                ButtonFocus focus = playerSprites[assignedControllers].GetComponent<ButtonFocus>();

                var actions = PlayerActions.CreateJoystickBindings();
                actions.Device = inputDevice;
                focus.Actions = actions;

                buttonsCursor.Add(focus);
                assignedControllers++;
            }
        }

        bool ThereIsNoPlayerUsingJoystick(InputDevice inputDevice)
        {
            return FindPlayerUsingJoystick(inputDevice) == null;
        }

        ButtonFocus FindPlayerUsingJoystick(InputDevice inputDevice)
        {
            var playerCount = buttonsCursor.Count;
            for (var i = 0; i < playerCount; i++)
            {
                var player = buttonsCursor[i];
                if (player.Actions.Device == inputDevice)
                {
                    return player;
                }
            }
            return null;
        }


        void OnDeviceDetached(InputDevice inputDevice)
        {
            var cursor = FindPlayerUsingJoystick(inputDevice);
            if (cursor != null)
            {
                RemovePlayer(cursor);
            }
        }

        void RemovePlayer(ButtonFocus cursor)
        {
            buttonsCursor.Remove(cursor);
            cursor.Actions = null;
            Destroy(cursor.gameObject);
        }
    }
}