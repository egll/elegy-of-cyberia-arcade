﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sound.RandomController;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour {

    public SoundMusicController MediumMusicController;
    public SoundMusicController HighMusicController;
    public AudioMixerSnapshot MediumMixerSnapshot;
    public AudioMixerSnapshot MainMixerSnapshot;
    [Range(0f, 10.0f)]
    public float MediumTransitionTime = 0;
    [Range(0f, 10.0f)]
    public float MainTransitionTime = 0;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Vial")
        {
            SoundMusicController.Trigger(MediumMusicController);
            SoundMusicController.Trigger(HighMusicController);
            MediumMixerSnapshot.TransitionTo(MediumTransitionTime);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Vial")
        {
            SoundMusicController.TriggerOff(MediumMusicController);
            SoundMusicController.TriggerOff(HighMusicController);
            MainMixerSnapshot.TransitionTo(MainTransitionTime);
        }
    }
}
