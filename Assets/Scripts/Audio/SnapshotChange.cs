﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SnapshotChange : MonoBehaviour {

    public AudioMixerSnapshot Menu;
    [Range(0f, 10.0f)]
    public float SnapshotTransition = 2f;
	// Use this for initialization
	void Start () {
        Menu.TransitionTo(SnapshotTransition);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
