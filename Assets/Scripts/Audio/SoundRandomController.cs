﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sound.RandomController
{
    [RequireComponent(typeof(AudioSource))]

    public class SoundRandomController : MonoBehaviour
    {

        public AudioClip[] Soundfiles;
        public AudioSource SoundEmitter;
        public bool RetriggerPrevention = true;

        public float PitchMinimum = 1f;
        public float PitchMaximum = 1f;

        public float Volumeminimum = 1f;
        public float Volumemaximum = 1f;
        // Use this for initialization
        void Start()
        {
            SoundEmitter.GetComponent<AudioSource>();
            if(SoundEmitter == null)
            {
                Debug.Log("Not found");
            }
        }
        private void PlaySoundClip()
        {
            float pitch = Random.Range(PitchMinimum, PitchMaximum);
            SoundEmitter.pitch = pitch;
            float volume = Random.Range(Volumeminimum, Volumemaximum);
            SoundEmitter.volume = volume;
        

            //picks a random nr from 1-to-Soundfiles.length. Moves clip to audio source(SoundEmitter) and plays that clip once.
            if (RetriggerPrevention)
            {
                int n = Random.Range(1, Soundfiles.Length);
                SoundEmitter.clip = Soundfiles[n];
                SoundEmitter.PlayOneShot(SoundEmitter.clip);
                Soundfiles[n] = Soundfiles[0];
                Soundfiles[0] = SoundEmitter.clip;
            }
            else
            {
                int n = Random.Range(0, Soundfiles.Length);
                SoundEmitter.clip = Soundfiles[n];
                SoundEmitter.PlayOneShot(SoundEmitter.clip);
            }
        }
        public static void Trigger(SoundRandomController src)
        {
         //   Debug.Log("getinput");
            if (src != null) src.PlaySoundClip();
         

        }

    }
}