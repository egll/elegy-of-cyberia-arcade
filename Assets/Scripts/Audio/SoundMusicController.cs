﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sound.RandomController
{
    [RequireComponent(typeof(AudioSource))]

    public class SoundMusicController : MonoBehaviour
    {

        public AudioClip Soundfiles;
        public AudioSource SoundEmitter;
        //  public bool looping = true;
        //public bool RetriggerPrevention = true;
        /*   public float PitchMinimum = 1f;
           public float PitchMaximum = 1f;

           public float Volumeminimum = 1f;
           public float Volumemaximum = 1f;*/
       [Range(0f,1.0f)]
              public float maxVolume;

              private float noVolume = 0f;
        [Range(0, 1)]
        public float Volume = 1f;
        [Range(.25f, 3)]
        public float Pitch = 1f;
        public bool Loop = true;
        [Range(0f, 1f)]
        public float SpacialBlend = 1f;

        void Awake()
        {
            SoundEmitter.GetComponent<AudioSource>();
            if (SoundEmitter == null)
            {
                Debug.Log("AudioSource Not found");
            }
        /*    SoundEmitter.loop = looping;
            SoundEmitter.volume = noVolume;
            SoundEmitter.clip = Soundfiles;*/

       
        }
        void Start()
        {
            //SoundEmitter.PlayOneShot(SoundEmitter.clip);
            Play();
        }
        public void SetSourceProperties(AudioClip clip, float volume, float pitch, bool loop, float spacialBlend)
        {
            SoundEmitter.clip = clip;
            SoundEmitter.volume = volume;
            SoundEmitter.pitch = pitch;
            SoundEmitter.loop = loop;
            SoundEmitter.spatialBlend = spacialBlend;
        }

        public void Play()
        {
            SetSourceProperties(Soundfiles, Volume, Pitch, Loop, SpacialBlend);
            SoundEmitter.Play();
        }
        private void SoundOn()
        {
            SoundEmitter.volume = maxVolume;
            Debug.Log("Music on input");
           
            //unused clip randomization code
            #region         
            /*  float pitch = Random.Range(PitchMinimum, PitchMaximum);
            SoundEmitter.pitch = pitch;
            float volume = Random.Range(Volumeminimum, Volumemaximum);
            SoundEmitter.volume = volume;


       //picks a random nr from 1-to-Soundfiles.length. Moves clip to audio source(SoundEmitter) and plays that clip once.
            if (RetriggerPrevention)
            {
                int n = Random.Range(1, Soundfiles.Length);
                SoundEmitter.clip = Soundfiles[n];
                SoundEmitter.PlayOneShot(SoundEmitter.clip);
                Soundfiles[n] = Soundfiles[0];
                Soundfiles[0] = SoundEmitter.clip;
            }
            else
            {
                int n = Random.Range(0, Soundfiles.Length);
                SoundEmitter.clip = Soundfiles[n];
                SoundEmitter.PlayOneShot(SoundEmitter.clip);
            }*/
            #endregion
        }
        private void SoundOff()
        {
            SoundEmitter.volume = noVolume;
            Debug.Log("Music off input");
        }
        public static void Trigger(SoundMusicController src)
        {
            //   Debug.Log("getinput");
            if (src != null) src.SoundOn();
            else
                {
                    Debug.Log("No Source");
                }
        }
        public static void TriggerOff(SoundMusicController src)
        {
            if (src != null) src.SoundOff();
        }
    }
}