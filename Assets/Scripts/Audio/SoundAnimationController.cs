﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sound.RandomController;

public class SoundAnimationController : MonoBehaviour {

    public SoundRandomController FootStepSound;
    public SoundRandomController JumpStepSound;
  //  public SoundRandomController JumpLandSound;
  //  public SoundRandomController DeathSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void StepSound()
    {
        SoundRandomController.Trigger(FootStepSound);
    }
    void JumpStep()
    {
        SoundRandomController.Trigger(JumpStepSound);
    }
}
