﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundSnapshotTrigger : MonoBehaviour {

    public AudioMixerSnapshot HighSnapshot;
    [Range(0.0f, 10.0f)]
    public float TimeTo;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Vial")
        {
           HighSnapshot.TransitionTo(TimeTo);
            Debug.Log("Trigger high");
        }
    }

}
