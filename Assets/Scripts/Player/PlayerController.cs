﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sound.RandomController;
using InControl;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public bool carrying = false;
    private PlayerCollisions playerCol;
    public GameObject arrow;

    public PlayerActions Actions { get; set; }
    public CharacterStats characterStats;
    private Rigidbody2D rb;
    private Collider2D characterCollider;

    [Header("Walking")]
    public SoundRandomController WalkingSoundRandom;
    public UnityEvent walkingEvent;
    private MOVEMENT facing;

    [Header("Jumping")]
    public SoundRandomController JumpSoundRandom;
    public UnityEvent jumpingEvent;

    [Header("Animations")]
    PlayerAnimations animations;

    [Header("Aiming")]
    public Transform crosshair;
    public Transform aimingCenter;
    public Transform hinge;

    [Header("Shooting")]
    public Transform weaponFront;
    public SoundRandomController AttackFx;
    public SoundRandomController AttackTailFx;
    public UnityEvent shootingEvent;
    private float currentShootingDelay;

    [Header("Dropping")]
    public UnityEvent droppingEvent;


    private void Awake()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
        playerCol = GetComponent<PlayerCollisions>();
        animations = GetComponent<PlayerAnimations>();
        characterCollider = GetComponent<Collider2D>();
        arrow = GameObject.Find(gameObject.tag + "Arrow");
    }

    private void LateUpdate()
    {
        if (Actions != null)
        {
            if (characterStats.IsAlive)
            {

            }
        }
    }
    private void FixedUpdate()
    {
        currentShootingDelay -= Time.fixedDeltaTime;
        if (Actions != null)
        {
            if (characterStats.IsAlive)
            {
                Move();
                RotateCharacter();
                Aim();
                Jumping();
                DropItem();
                Shoot();
            }
        }

        if (arrow)
        {
            if (carrying)
            {
                arrow.GetComponent<Image>().enabled = true;
            }
            else
            {
                arrow.GetComponent<Image>().enabled = false;
            }
        }
    }

    void RotateCharacter()
    {
        facing = MoveCharacter.Facing(rb);
        Vector3 previousAiming = crosshair.transform.position;
        if (facing != MOVEMENT.STATIC)
        {
            if (facing == MOVEMENT.RIGHT)
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);

            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }

    public void Move()
    {
        MoveCharacter.StartAction(rb, Actions.Movement.Value * characterStats.Speed);
    }

    public void Aim()
    {
        MoveCrosshair();
        MoveHinge();

    }

    public void Jumping()
    {
        if (Actions.Jump.WasPressed)
        {
            if (Jump.StartAction(rb, characterStats.JumpForce))
            {
                SoundRandomController.Trigger(JumpSoundRandom);
                jumpingEvent.Invoke();
            }
        }
    }

    public void DropItem()
    {
        if (Actions.Drop.WasPressed && carrying)
        {
            playerCol.DeAttachFromBackpack();
            droppingEvent.Invoke();
        }
    }

    public void Shoot()
    {
        if ((Actions.Shoot.WasPressed || Actions.Shoot.WasRepeated) && currentShootingDelay <= 0)
        {
            animations.AttackAnimation();
            currentShootingDelay = characterStats.ShootDelay;
            if (weaponFront != null)
            {
                var dir = crosshair.transform.position - hinge.transform.position;
                var angle = Mathf.Atan2(IsCharacterFacingRight() ? -dir.y : dir.y, dir.x) * Mathf.Rad2Deg;
                Instantiate(characterStats.Bullet, weaponFront.transform.position, Quaternion.AngleAxis(angle, weaponFront.forward));
            }
            else
            {
                Instantiate(characterStats.Bullet, crosshair.transform.position, crosshair.transform.rotation);
            }
            SoundRandomController.Trigger(AttackFx);
            SoundRandomController.Trigger(AttackTailFx);
            shootingEvent.Invoke();
        }
    }

    public bool IsCharacterFacingRight()
    {
        return transform.rotation.eulerAngles.y != 0;
    }

    public void StopMotion()
    {
        characterCollider.enabled = false;
        rb.velocity = Vector2.zero;
        rb.Sleep();
    }

    public void StartMotion()
    {
        characterCollider.enabled = true;
        rb.WakeUp();
    }

    public void WalkingEffect()
    {
        SoundRandomController.Trigger(WalkingSoundRandom);
        walkingEvent.Invoke();
    }

    private void MoveCrosshair()
    {
        Vector3 movement = new Vector3(Actions.AimRotation.Value.x, Actions.AimRotation.Value.y, 0);
        if (!IsCharacterFacingRight())
        {
            movement.x = -movement.x;
        }

        Vector3 newPos = crosshair.transform.localPosition + movement;
        Vector3 offset = newPos - aimingCenter.transform.localPosition;
        crosshair.transform.localPosition = aimingCenter.transform.localPosition + Vector3.ClampMagnitude(offset, characterStats.AimingRadius);
    }

    private void MoveHinge()
    {
        if (hinge != null)
        {
            var dir = aimingCenter.transform.position - crosshair.transform.position;
            var angle = Mathf.Atan2(IsCharacterFacingRight() ? -dir.y : dir.y, dir.x) * Mathf.Rad2Deg;
            hinge.rotation = Quaternion.Euler(Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles + RotateArm());
        }
    }

    private Vector3 RotateArm()
    {
        Vector3 rotationAngle = Vector3.zero;
        if (IsCharacterFacingRight())
        {
            rotationAngle = new Vector3(180, 0, 0);
        }
        return rotationAngle;
    }
}
