﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sound.RandomController;

public class PlayerCollisions : MonoBehaviour {

    public Transform backpack;

    public GameObject goal;
    public SoundRandomController VialTake;
    public GameObject vial;

    public PlayerController playercontrol;
    private PlayerStats playerstats;
    public CharacterStats characterStats;

    private float damageTimer = 0.5f;

    private void Awake()
    {
        playerstats = gameObject.GetComponent<PlayerStats>();
        GetGoal();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Vial" && !playercontrol.carrying)
        {
            Debug.Log("Picked up item");
            MoveToBackpack(collision.gameObject);
            SoundRandomController.Trigger(VialTake);
        }

        if(collision.gameObject.tag == "Projectile")
        {
            Debug.Log("Ouch");
            gameObject.SendMessage("TakeDamage", 5f);
        }


        if (collision.gameObject.tag == "Goal" && playercontrol.carrying)
        {
            if(collision.gameObject == goal)
            {
                DeAttachFromBackpack();
                GameObject vial = GameObject.FindGameObjectWithTag("Vial");
                vial.GetComponent<Vial>().ReturnVial();
                playerstats.Score();
            }
        }
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Sheep")
        {
            damageTimer -= Time.deltaTime;
            gameObject.SendMessage("TakeDamage", 1f);
            collision.gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "EnvironmentDamage")
        {
            damageTimer -= Time.deltaTime;
            if(damageTimer <= 0)
            {
                damageTimer = 1f;
                GetBurnt();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Vial") && playercontrol.carrying)
        {
            DeAttachFromBackpack();
        }
    }

    void MoveToBackpack(GameObject item)
    {
        playercontrol.carrying = true;
        item.transform.position = backpack.transform.position;
        item.transform.SetParent(backpack.transform, true);
    }

    public void DeAttachFromBackpack()
    {
        if (backpack.transform.childCount > 0 && playercontrol.carrying)
        {
            Transform carryingItem = backpack.transform.GetChild(0);
            GameObject item = carryingItem != null ? carryingItem.gameObject : null;
            item.transform.SetParent(null);
        }
        playercontrol.carrying = false;
    }

    void ChangeSpeed(float NewSpeed)
    {
        characterStats.Speed = NewSpeed;
    }

    void GetGoal()
    {
        goal = GameObject.Find(gameObject.tag + " Goal");
    }

    void GetBurnt()
    {
        GetComponent<PlayerStats>().TakeDamage(10f);
        GameObject fire = Resources.Load("Prefabs/FireOuch") as GameObject;
        Instantiate(fire, new Vector3(transform.position.x, transform.position.y - 3f, transform.position.z), Quaternion.identity);
        ParticleSystem parts = fire.GetComponent<ParticleSystem>();
        float totalDuration = parts.duration + parts.startLifetime;
    }
}
