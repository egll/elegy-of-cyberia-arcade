﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sound.RandomController;
using System;

public class PlayerStats : MonoBehaviour {
    public CharacterStats characterStats;
    public SoundRandomController HurtSound;
    public SoundRandomController DeathSound;
    public SoundRandomController WarcrySound;
    private PlayerController playerController;
    private PlayerCollisions playerCollisions;
    private GameObject scoreCount;

    private GameObject[] respawns;

    public GameObject spawnVFX;
    public SpriteRenderer head;
    public SpriteRenderer skull;

    private void Awake()
    {
        SetHead(head, true, skull, false);
        playerController = GetComponent<PlayerController>();
        playerCollisions = GetComponent<PlayerCollisions>();
        scoreCount = GameObject.Find(gameObject.tag + "Score");
        respawns = GameObject.FindGameObjectsWithTag("Spawnpoint");
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.F1))
        {
            characterStats.HP = 0;
            Debug.Log(gameObject.name + characterStats.HP);
            gameObject.SendMessage("TakeDamage", 100);
        }
        if(transform.position.y < -15f)
        {
            
            if (playerController.carrying)
            {
                playerCollisions.DeAttachFromBackpack();
                
            }
            Die();
        }
        if (!Vial.instance.InRange(transform))
        {
            Spawn();
        }
    }

    public void TakeDamage(float damage)
    {
        characterStats.HP -= damage;
        if(characterStats.HP <= 0 && characterStats.IsAlive)
        {
            characterStats.IsAlive = false;
            SoundRandomController.Trigger(DeathSound);
            Die();
        }
        else
        {
            if (characterStats.IsAlive)
            {
                SoundRandomController.Trigger(HurtSound);
            }
        }
    }



    void Die()
    {
        SetHead(head, false, skull, true);
        characterStats.IsAlive = false;
        Debug.Log(gameObject.name + " Ur dead as shit");
        playerController.StopMotion();
        StartCoroutine(ReSpawn(3f, gameObject));
        gameObject.SendMessage("DieAnimation");
        playerCollisions.DeAttachFromBackpack();
    }


    void Spawn()
    {
        gameObject.SendMessage("ResetAnimation");
        transform.position = GetSpawnpoint().transform.position;
        if (spawnVFX)
        {
            Instantiate(spawnVFX, transform.position, Quaternion.identity);
        }
        SetHead(head, true, skull, false);
        characterStats.ResetStats();
        SoundRandomController.Trigger(WarcrySound);
        playerController.StartMotion();
    }

    public void Score()
    {
        if (characterStats.CharacterScore == 2)
        {
            StartCoroutine(WinningMatch());
        }
        else
        {
            characterStats.CharacterScore++;
            scoreCount.GetComponent<ScoreCounter>().UpdateScore(characterStats.CharacterScore);
            Debug.Log(characterStats.CharacterScore);
        }
    }

    private IEnumerator WinningMatch()
    {
        yield return new WaitForSecondsRealtime(2f);
        GameController.instance.GameWon(gameObject);
    }

    public IEnumerator ReSpawn(float delay, GameObject player)
    {
        Debug.Log("Starting coroutine");
        
        yield return new WaitForSeconds(delay);
        Spawn();
    }

    private GameObject GetSpawnpoint()
    {
        GameObject closest = null;
        float distance = Mathf.Infinity;

        foreach (GameObject spawnpoint in respawns){
            Vector3 diff = spawnpoint.transform.position - Vial.instance.GetPosition();
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = spawnpoint;
                distance = curDistance;
            }
        }
        return closest;
    }

    void SetHead(SpriteRenderer head, bool _headvalue, SpriteRenderer skull, bool _skullvalue)
    {
        head.GetComponent<SpriteRenderer>().enabled = _headvalue;
        skull.GetComponent<SpriteRenderer>().enabled = _skullvalue;
    }

}
