﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour {

    public Animator anims;
    private Rigidbody2D rb;
    //private PlayerController player;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        //player = GetComponent<PlayerController>();
    }
    private void Update()
    {
        //Debug.Log(rb.velocity.x);
        if(rb.velocity.x != 0)
        {
            anims.SetBool("Running", true);

        } else {
            anims.SetBool("Running", false);
        }
        if (Jump.IsInGround(rb))
        {
            anims.SetBool("Jumping", false);
        } else {
            anims.SetBool("Jumping", true);
        }
    }

    public void DieAnimation()
    {
        anims.SetBool("Dead", true);
    }
    public void ResetAnimation()
    {
        anims.SetBool("Dead", false);
    }

    public void AttackAnimation()
    {
        anims.SetBool("Attack", true);
    }

    public void AttackEnded()
    {
        anims.SetBool("Attack", false);
    }

    public void HeadExplode()
    {
        GameObject decapitation = Resources.Load("Prefabs/Decapitation") as GameObject;
        Instantiate(decapitation, transform.position, Quaternion.identity);
    }

    public void GhostAppear(string ghostName)
    {
        GameObject ghost = Resources.Load("Prefabs/Characters/"+ghostName+ "/"+ ghostName+(" (Ghost)")) as GameObject;
        Instantiate(ghost, transform.position, Quaternion.identity);
    }

}
