﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGhost : MonoBehaviour {

    public string GhostOf;
    private GameObject target;
    private SpriteRenderer[] sprites;

    private void Awake()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>();
        transform.localScale = GetRotation();
    }

    private void Update()
    {
        FadeOut();
        FloatUp();
    }

    void FadeOut()
    {
        foreach(SpriteRenderer sprite in sprites)
        {
            Color color = sprite.material.color;
            color.a -= 0.007f;
            if(color.a <= 0)
            {
                Destroy(gameObject);
            }
            sprite.material.color = color;
        }
    }

    Vector3 GetRotation()
    {
        target = GameObject.Find(GhostOf);
        if(target.transform.localScale.x == 1)
        {
            return new Vector3(-1, 1, 1);
        } else {
            return new Vector3(1, 1, 1);
        }
    }

    void FloatUp()
    {
        transform.Translate(Vector3.up * Time.deltaTime * 2);
    }


}
