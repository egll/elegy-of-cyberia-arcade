﻿using UnityEngine.UI;
using UnityEngine;

public class Fader : MonoBehaviour {

    public static Fader instance;
    private Image blackScreen;

    private void Awake()
    {
        blackScreen = GetComponent<Image>();
    }

    void FadeToClear()
    {
        blackScreen.color = Color.Lerp(blackScreen.color, Color.clear, 1.5f * Time.deltaTime);
    }


    void FadeToBlack()
    {
        blackScreen.color = Color.Lerp(blackScreen.color, Color.black, 1.5f * Time.deltaTime);
    }

}
