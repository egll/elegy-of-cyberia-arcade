﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {

    public GameObject main;
    public GameObject instructions;
    public GameObject charscreen;

    public string Player1Char;
    public string Player2Char;

    private GameObject SelectVFX;


    public void InstructionsOn()
    {
        main.gameObject.SetActive(false);
        instructions.gameObject.SetActive(true);
    }

    public void InstructionsOff()
    {
        main.gameObject.SetActive(true);
        instructions.gameObject.SetActive(false);
    }

    public void CharScreen()
    {
        main.gameObject.SetActive(false);
        charscreen.SetActive(true);
    }

    //Gonna need to do something related to playerIDs when it comes to determining if player 1 or player 2 picked

    public void CharacterSelected(Button button)
    {
        Debug.Log("Player1: " + button.name);
        Player1Char = button.name;
        PlayerPrefs.SetString("Player1", Player1Char);
        //button.GetComponentInChildren<Animator>().SetTrigger("Selected");
        GameObject SelectVFX = Resources.Load("Effects/PlasmaExplosionEffectMenu") as GameObject;
        Instantiate(SelectVFX, new Vector3(button.transform.position.x, button.transform.position.y + 55f), Quaternion.identity);
    }


}
