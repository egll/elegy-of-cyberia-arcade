﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Healthbar : MonoBehaviour {

    public GameObject player;
    public Image bar;

    private float health;
    private CharacterStats stats;

    private void OnEnable()
    {
        stats = player.GetComponent<PlayerController>().characterStats;
    }

    void Update()
    {
        bar.fillAmount = stats.HP / stats.InitialHP;
    }

}
