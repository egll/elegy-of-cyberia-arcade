﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swarm : MonoBehaviour {

    private Rigidbody2D rb;
    public float damage = 35f;
    public float lifetime = 15f;
    public float force = 4f;
    public GameObject swarmEffect;
    public GameObject explosionEffect;
    public float delay = 1f;
    private Collider2D swarmCollider;

    private void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
        swarmCollider = GetComponent<Collider2D>();
        rb.velocity = transform.right * force;
        Destroy(gameObject, lifetime);
        swarmEffect.SetActive(true);
        explosionEffect.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("P1"))
        {
            rb.velocity = Vector2.zero;
            swarmCollider.enabled = false;
            collision.gameObject.GetComponent<PlayerStats>().TakeDamage(damage);
            swarmEffect.SetActive(false);

            //explosionEffect.SetActive(true);
            //yield return new WaitForSeconds(delay);
        }

        if (collision.gameObject.tag == "Sheep")
        {
            Debug.Log("Test");
            collision.gameObject.GetComponent<Sheep>().TakeDamage(damage);
        }
    }

}
