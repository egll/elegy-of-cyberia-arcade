﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GameController : MonoBehaviour
{

    public static GameController instance;
    public float GameTime = 60f;
    public bool gameover;
    public AudioSource SheepSource;
    public AudioSource music;
    public AudioSource ScorePoint;
    public AudioClip PointScoredClip;
    public AudioClip Sheep;
    public AudioMixerSnapshot Starting;
    public AudioMixerSnapshot InGame;
    public AudioMixerSnapshot Victory;
    public AudioClip EndMusic;

    private bool isplaying = false;
    private bool matchstart = true;
    private bool sheepSpawning = false;

    public GameObject MechariderWin;
    public GameObject CyfenWin;
    public GameObject KarelianWin;
    public GameObject BlackMarketWin;

    public GameObject arrowCanvas;
    public GameObject UICanvas;

    public Image FadeImg;
    private bool sceneStarting;

    [Header("Go to main menu")]
    public bool RestartToMainMenu;

    public List<GameObject> sheeps = new List<GameObject>();

    void Start()
    {
        instance = this;
        Time.timeScale = 1.5f;
        InGame.TransitionTo(2f);
    }
    private void Update()
    {
        GameTime -= Time.deltaTime;
        if (GameTime < 5f && !isplaying)
        {
            SheepSource.PlayOneShot(Sheep);
            isplaying = true;
        }
        if (GameTime < 0.0f)
        {
            gameover = true;
            StartSpawning();
        }
    }

    public void GameWon(GameObject player)
    {
        CameraController.instance.ChangeTarget(player);
        Vial.instance.ReturnVial();
        Debug.Log(player.name + " won");
        arrowCanvas.SetActive(false);
        UICanvas.SetActive(false);
        if (EndMusic != null)
        {
            music.Stop();
            music.PlayOneShot(EndMusic);
        }
        else
        {
            Debug.Log("Music_not_found");
        }
        Victory.TransitionTo(1f);
        if (player.name.Contains("Mecharider"))
        {
            MechariderWin.SetActive(true);
        }
        if (player.name.Contains("Cyfen"))
        {
            CyfenWin.SetActive(true);
        }
        if (player.name.Contains("Karelian"))
        {
            KarelianWin.SetActive(true);
        }
        if (player.name.Contains("BlackMarket"))
        {
            BlackMarketWin.SetActive(true);
        }
        Invoke("Restart", 6f);

    }

    void Restart()
    {
        if (RestartToMainMenu)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

    }

    void StartSpawning()
    {
        if (!sheepSpawning)
        {
            sheepSpawning = true;
            InvokeRepeating("SpawnSheep", 1.5f, 3.5f);
        }
    }

    void SpawnSheep()
    {
        sheeps.RemoveAll(item => item == null);
        // Should probably use pooling, but fuck it
        if (sheeps.Count <= 10)
        {
            GameObject sheep = Resources.Load("Prefabs/SHEEP_OMNOM") as GameObject;
            sheeps.Add(Instantiate(sheep, transform.position, Quaternion.identity));
        }
    }

}
