﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

    public GameController gc;

    private Text counter;

    private void Awake()
    {
        counter = gameObject.GetComponent<Text>();
    }

    private void FixedUpdate()
    {
        counter.text = gc.GameTime.ToString("f0");
        if(gc.GameTime < 30)
        {
            counter.color = Color.red;
        }
        if(gc.GameTime < 10)
        {
            counter.color = Color.Lerp(Color.red, new Color(0,0,0,0) , Mathf.PingPong(Time.time, 0.5f));
        }
        if(gc.GameTime <= 0)
        {
            counter.text = "0";
            return;
        }
    }

}
