﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{

    [Range(1, 2)]
    public int playerNR;
    public GameObject ScoreCount;

    private Sprite[] pointsSprites;
    private GameObject player;
    private GameObject ScoreDots;

    private void Awake()
    {
        if (player == null)
        {
            GetPlayer(playerNR);
        }
        if (ScoreDots == null)
        {
            ScoreCount.GetComponent<SpriteRenderer>();
        }
    }

    private void Start()
    {
        if (pointsSprites == null)
        {
            pointsSprites = new Sprite[3];
            for (int i = 0; i < pointsSprites.Length; i++)
            {
                pointsSprites[i] = Resources.Load<Sprite>("Images/UI/Counter/" + player.name + (i + 1));
            }
        }
    }

    void GetPlayer(int number)
    {
        player = GameObject.FindGameObjectWithTag("P" + number);
        Debug.Log(player.name);
    }

    public void UpdateScore(float score)
    {
        ScoreCount.GetComponent<Image>().sprite = pointsSprites[(int)score -1];
    }

}
