﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    public PlayerController player;
    public GameObject goal;
    public GameObject vial;

    private Transform target;

    private void Awake()
    {
        target = vial.transform;
    }

    private void Update()
    {
        RotateTowards();
        Debug.Log(player.carrying);
        if (player.carrying)
        {
            target = goal.transform;
        } else {
            target = vial.transform;
        }
    }

    void RotateTowards()
    {
        float angle = 0;

        Vector3 relative = transform.InverseTransformPoint(target.position);
        angle = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
        transform.Rotate(0, 0, -angle);
    }

}
