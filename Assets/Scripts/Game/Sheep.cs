﻿using UnityEngine;
using Sound.RandomController;

public class Sheep : MonoBehaviour {

    public GameObject[] spawns;
    public PlayerController[] targets;
    public SoundRandomController SheepDeathController;

    public float _health;

    private Rigidbody2D rb;
    private PlayerController target;
    private float health;

    private void Awake()
    {
        health = _health;
        rb = GetComponent<Rigidbody2D>();
        GetSpawn();
        GetTarget();
    }

    void FixedUpdate() {
        if (target != null)
        {
            ChaseTarget();
            GetComponent<SpriteRenderer>().flipX = GetVelocity();
        }
    }

    void GetSpawn()
    {
        spawns = GameObject.FindGameObjectsWithTag("Spawnpoint");
        int i = Random.Range(0, spawns.Length);
        GameObject spawnpoint = spawns[i];
        transform.position = spawnpoint.transform.position;
    }

    void GetTarget()
    {
        targets = (PlayerController[])GameObject.FindObjectsOfType(typeof(PlayerController));
        int i = Random.Range(0, targets.Length);
        target = targets[i];
    }

    void ChaseTarget()
    {
        Vector3 vectorToTarget = target.gameObject.transform.position - transform.position;
        rb.velocity = vectorToTarget * 15 * Time.deltaTime;
    }

    bool GetVelocity()
    {
        if (rb.velocity.x <= 0) { return false; } else return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Swarm of Biodromes"))
        {
            Debug.Log("Test");
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Die();
            SoundRandomController.Trigger(SheepDeathController);
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
