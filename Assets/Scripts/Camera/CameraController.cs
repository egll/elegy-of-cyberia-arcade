﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    public float interpVelocity;
    public float minDistance;
    public float followDistance;
    public GameObject target;
    public Vector3 offset;
    Vector3 targetPos;
    // Use this for initialization
    void Start()
    {
        targetPos = transform.position;
        var pos = transform.position;
        instance = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 posNoZ = transform.position;
            posNoZ.z = target.transform.position.z;

            Vector3 targetDirection = (target.transform.position - posNoZ);

            interpVelocity = targetDirection.magnitude * 6f;

            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, 0.25f);

            var pos = transform.position;
            transform.position = new Vector3(
                Mathf.Clamp(pos.x, -63f, 89f),
                Mathf.Clamp(pos.y - 0.15f, -1.0f, 2.75f),
                pos.z
            );
        }
    }

    public void ChangeTarget(GameObject NewTarget)
    {
        target = NewTarget;
    }
}
