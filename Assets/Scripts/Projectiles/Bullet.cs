﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Rigidbody2D rb;
    public float speed;

    void Awake(){
        if(rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
    }

    void Update () {

        rb.velocity = transform.forward * speed;

	}

}
