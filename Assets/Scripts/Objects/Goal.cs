﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

    public GameObject player;
    public GameController gc;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject == player)
        {
            if (other.gameObject.GetComponent<PlayerCollisions>()){
                Vial.instance.ResetVial();
            }
        }
    }

    private void Update()
    {
        if(player == null)
        {
            GetPlayer();
        }
    }

    void GetPlayer()
    {
        if(gameObject.name == "P1 Goal")
        {
            player = GameObject.FindGameObjectWithTag("P1");
        } else {
            player = GameObject.FindGameObjectWithTag("P2");
        }
    }

}
