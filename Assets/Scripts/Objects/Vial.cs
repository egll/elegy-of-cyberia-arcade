﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vial : MonoBehaviour
{

    public static Vial instance;
    private Vector3 startPos;
    private Rigidbody2D rb;
    private Collider2D vialCollider;
    private GameObject carrier;
    private PlayerController carrierController;

    private void Awake()
    {
        startPos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        vialCollider = GetComponent<Collider2D>();
        instance = this;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("P1") || other.gameObject.CompareTag("P2"))
        {
            carrier = other.gameObject;
            carrierController = carrier.GetComponent<PlayerController>();
            if (carrierController != null)
            {
                carrierController.carrying = true;
            }
            
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("P1") || other.gameObject.CompareTag("P2"))
        {
            if (carrier == other.gameObject)
            {
                if (carrierController != null)
                {
                    carrierController.carrying = false;
                }
                carrier = null;
                carrierController = null;
            }
            PlayerController controller = other.gameObject.GetComponent<PlayerController>();
            if (controller != null)
            {
                controller.carrying = false;
            }
        }
    }

    public void ReturnVial()
    {
        gameObject.transform.SetParent(null);

    }

    private void Update()
    {
        if (transform.root == transform)
        {
            rb.angularVelocity = Random.value * 25f;
        }
        else
        {
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            return;
        }
        if (transform.position.y < -15f)
        {
            ResetVial();
        }
    }

    IEnumerator ToggleColliderTemporarily()
    {
        vialCollider.enabled = false;
        yield return new WaitForSeconds(2f);
        vialCollider.enabled = true;
    }

    public bool InRange(Transform target)
    {
        Vector3 diff = transform.position - target.transform.position;
        float curDistance = diff.sqrMagnitude;
        if (curDistance < 3300)  // Value came from trial and error
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void ResetVial()
    {
        if (transform.parent != null)
        {
            gameObject.transform.SetParent(null);
        }
        transform.position = startPos;
    }

}
